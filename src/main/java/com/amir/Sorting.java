package com.amir;

import java.util.Arrays;
import java.util.Scanner;

public class Sorting {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(sortNumbers(scanner.nextLine()));
    }

    public static String sortNumbers(String input) {

        String[] strNumbers = input.trim().split(" ");
        int[] intNumbers = new int[strNumbers.length];
        try {
            for (int i = 0; i < strNumbers.length; i++) {
                intNumbers[i] = Integer.parseInt(strNumbers[i]);
            }
        } catch (IllegalArgumentException e) {
            return "Wrong input";
        }
        if (intNumbers.length > 10) {
            return "The number of arguments exceeded";
        }
        Arrays.sort(intNumbers);
        StringBuilder result = new StringBuilder();
        for (int intNumber : intNumbers) {
            result.append(intNumber).append(" ");
        }
        return result.toString().trim();
    }
}
