package com.amir;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SortingOneArgumentCaseTest {
    private String input;
    private String expected;

    public SortingOneArgumentCaseTest(String input) {
        this.input = input;
        this.expected = input;
    }

    @Test
    public void OneArgumentCase() {
        assertEquals(expected, Sorting.sortNumbers(input));
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {"5"},
                {"7"}
        });
    }
}
