package com.amir;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SortingNormalArgumentsCaseTest {
    private String input;
    private String expected;

    public SortingNormalArgumentsCaseTest(String input, String expected) {
        this.input = input;
        this.expected = expected;
    }

    @Test
    public void normalCase() {
        assertEquals(expected, Sorting.sortNumbers(input));
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {"123 115 17 20 0 55", "0 17 20 55 115 123"},
                {"1 2 5 3 4", "1 2 3 4 5"},
                {"159 55 7 8 0 31 1 2 98 100", "0 1 2 7 8 31 55 98 100 159"},
                {"18 14", "14 18"}
        });
    }
}
