package com.amir;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.junit.Assert.assertEquals;

public class SortingZeroArgumentCaseTest {

    @Test
    public void zeroArgumentCase() {
        assertEquals("Wrong input", Sorting.sortNumbers("       "));
    }
}
