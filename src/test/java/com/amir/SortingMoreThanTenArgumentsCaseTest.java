package com.amir;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SortingMoreThanTenArgumentsCaseTest {
    private String input;

    public SortingMoreThanTenArgumentsCaseTest(String input) {
        this.input = input;
    }

    @Test
    public void moreThanTenArgumentsCase() {
        assertEquals("The number of arguments exceeded", Sorting.sortNumbers(input));
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {"159 55 7 8 0 31 1 2 98 100 555"},
                {"18 14 20 16 12 10 4 2 6 8 40"}
        });
    }
}
