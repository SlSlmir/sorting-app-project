package com.amir;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SortingTenArgumentsCaseTest {
    private String input;
    private String expected;

    public SortingTenArgumentsCaseTest(String input, String expected) {
        this.input = input;
        this.expected = expected;
    }

    @Test
    public void tenArgumentsCase() {
        assertEquals(expected, Sorting.sortNumbers(input));
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {"159 55 7 8 0 31 1 2 98 100", "0 1 2 7 8 31 55 98 100 159"},
                {"18 14 20 16 12 10 4 2 6 8", "2 4 6 8 10 12 14 16 18 20"}
        });
    }
}
